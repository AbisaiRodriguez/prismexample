﻿using System;
using System.Collections.Generic;
using System.Linq;

using Prism;
using Foundation;
using UIKit;
using Prism.Ioc;

namespace PrismExample.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();

            LoadApplication(new App(new iOSInitilaizer()));

            return base.FinishedLaunching(app, options);
        }
    }

    public class iOSInitilaizer : IPlatformInitializer
    {
        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
          
        }
    }

}
