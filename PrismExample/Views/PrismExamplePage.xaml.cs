﻿using Xamarin.Forms;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismExample.Views
{
    public partial class PrismExamplePage : ContentPage
    {
        public PrismExamplePage()
        {
            Debug.WriteLine($"****{this.GetType().Name}; ctor");
            InitializeComponent();

        }
    }
}
