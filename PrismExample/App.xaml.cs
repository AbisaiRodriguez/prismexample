﻿using Prism;
using Prism.Unity;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Prism.Ioc;
using PrismExample.Views;
using PrismExample.ViewModels;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace PrismExample
{
    public partial class App : PrismApplication
    {
        public App() : this(null) {}

        public App(IPlatformInitializer initializer = null) : base(initializer){ }

        protected override async void OnInitialized()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnInitialized)}");
            InitializeComponent();

            await NavigationService.NavigateAsync($"{nameof(NavigationPage)}/{nameof(PrismExamplePage)}");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(RegisterTypes)}");
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<PrismExamplePage, PrismExamplePageViewModel>();


        }

        protected override void OnStart()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnStart)}");
            // Handle when your app starts
            base.OnStart();
        }

        protected override void OnSleep()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnSleep)}");
            // Handle when your app sleeps
            base.OnSleep();
        }

        protected override void OnResume()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnResume)}");
            // Handle when your app resumes
            base.OnResume();
        }
    }
}
