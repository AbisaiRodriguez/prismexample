﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Prism.Navigation;
using Prism.Commands;

namespace PrismExample.ViewModels
{
    public class PrismExamplePageViewModel : BindableBase, INavigationAware
    {

        INavigationService _navigationService;

        public DelegateCommand NavToAwesomePageCommand { get; set; }

        private string _title;

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public PrismExamplePageViewModel(INavigationService navigationService)
        {
            Debug.WriteLine($"****{this.GetType().Name}: ctor");
            Title = "Main Page";
            _navigationService = navigationService;
            NavToAwesomePageCommand = new DelegateCommand(OnNavToAwesomePage);


        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedFrom)}");
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedTo)}");
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatingTo)}");
        }

        private void OnNavToAwesomePage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavToAwesomePage)}");
        }


    }
}